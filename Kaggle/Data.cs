namespace Kaggle;

public class Data
{
    public int hotel { get; set; } //enum
    public int is_canceled { get; set; } //bool
    public int lead_time { get; set; }
    private int arrival_date_year { get; set; }
    private string arrival_date_month { get; set; }
    private int arrival_date_week_number { get; set; }
    private int arrival_date_day_of_month { get; set; }
    public DateTime arrival_date { get; set; }
    public int stays_in_weekend_nights { get; set; }
    public int stays_in_week_nights { get; set; }
    public int? adults { get; set; }
    public int? children { get; set; }
    public int? babies { get; set; }
    public int meal { get; set; } //enum
    public string country { get; set; }
    public int market_segment { get; set; }
    public int distribution_channel { get; set; }
    public int is_repeated_guest { get; set; } //bool
    public int previous_cancellations { get; set; }
    public int previous_bookings_not_canceled { get; set; }
    public string reserved_room_type { get; set; }
    public string assigned_room_type { get; set; }
    public int booking_changes { get; set; }
    public int deposit_type { get; set; }
    public int? agent { get; set; }
    public int? company { get; set; }
    public int days_in_waiting_list { get; set; }
    public int customer_type { get; set; } //enum
    public double adr { get; set; }
    public int required_car_parking_spaces { get; set; }
    public int total_of_special_requests { get; set; }
    public int reservation_status { get; set; } //enum
    public DateTime reservation_status_date { get; set; }

    public Data()
    {
        //
    }
    
    public Data(string[] input)
    {
        hotel = input[0] switch
        {
            "Resort Hotel" => 1,
            "City Hotel" => 2,
            _ => 0
        };
        is_canceled = input[1] == "1" ? 1 : 0;
        lead_time = int.Parse(input[2]);
        arrival_date_year = int.Parse(input[3]);
        arrival_date_month = input[4];
        arrival_date_week_number = int.Parse(input[5]);
        arrival_date_day_of_month = int.Parse(input[6]);
        arrival_date = new DateTime(arrival_date_year, MonthToInt(arrival_date_month), arrival_date_day_of_month);
        stays_in_weekend_nights = int.Parse(input[7]);
        stays_in_week_nights = int.Parse(input[8]);
        adults = input[9] == "NA" ? null : int.Parse(input[9]);
        children = input[10] == "NA" ? null : int.Parse(input[10]);
        babies = input[11] == "NA" ? null :  int.Parse(input[11]);
        meal = input[12] switch
        {
            "BB" => 1,
            "HB" => 2,
            "FB" => 3,
            _ => 0
        };
        country = input[13];
        market_segment = input[14] switch
        {
            "Direct" => 1,
            "Corporate" => 2,
            "Online TA" => 3,
            "Offline TA/TO" => 4,
            _ => 0
        };
        distribution_channel = input[15] switch
        {
            "Direct" => 1,
            "Corporate" => 2,
            "TA/TO" => 3,
            _ => 0
        };
        is_repeated_guest = input[16] == "1" ? 1 : 0;
        previous_cancellations = int.Parse(input[17]);
        previous_bookings_not_canceled = int.Parse(input[18]);
        reserved_room_type = input[19];
        assigned_room_type = input[20];
        booking_changes = int.Parse(input[21]);
        deposit_type = input[22] switch
        {
            "No Deposit" => 1,
            "Non Refund" => 2,
            _ => 0
        };
        agent = input[23] == "NULL" ? null : int.Parse(input[23]);
        company = input[24] == "NULL" ? null : int.Parse(input[24]);
        days_in_waiting_list = int.Parse(input[25]);
        customer_type = input[26] switch
        {
            "Transient" => 1,
            "Transient-Party" => 2,
            "Contract" => 3,
            _ => 0
        };
        adr = double.Parse(input[27].Replace('.',','));
        required_car_parking_spaces = int.Parse(input[28]);
        total_of_special_requests = int.Parse(input[29]);
        reservation_status = input[30] switch
        {
            "Check-Out" => 1,
            "Canceled" => 2,
            _ => 0
        };
        reservation_status_date = DateTime.Parse(input[31]);
    }

    private static int MonthToInt(string month)
    {
        return month switch
        {
            "January" => 1,
            "February" => 2,
            "March" => 3,
            "April" => 4,
            "May" => 5,
            "June" => 6,
            "July" => 7,
            "August" => 8,
            "September" => 9,
            "October" => 10,
            "November" => 11,
            "December" => 12,
            _ => -1,
        };
    }
}