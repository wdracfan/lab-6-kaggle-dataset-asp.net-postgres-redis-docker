﻿namespace Kaggle.DAL;

public class ReservationModel
{
    public int reservation_status { get; set; }
    public DateTime reservation_status_date { get; set; }
}
