﻿using System.Text.Json;
using StackExchange.Redis;

namespace Kaggle.DAL;

public static class Redis
{
    public static async Task<string> CheckRedis(IDatabase db, object request)
    {
        var req = JsonSerializer.Serialize(request);
        if (await db.KeyExistsAsync(req))
        {
            return await db.StringGetAsync(req);
        }

        return "";
    }

    public static async Task CacheRedis(IDatabase db, object request, object response)
    {
        await db.StringSetAsync(JsonSerializer.Serialize(request), JsonSerializer.Serialize(response));
    }

    public static async Task ClearRedis(IServer server)
    {
        await server.FlushDatabaseAsync();
    }
}