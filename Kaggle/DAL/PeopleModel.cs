﻿namespace Kaggle.DAL;

public class PeopleModel
{
    public int adults { get; set; }
    public int children { get; set; }
    public int babies { get; set; }
    public DateTime arrival_date { get; set; }
}