﻿namespace Kaggle.DAL;

public class FilterRequest
{
    public class Parameters
    {
        public int? Hotel { get; set; }
        public Date? DateStart { get; set; }
        public Date? DateEnd { get; set; }
        public string? Country { get; set; }

        public Parameters()
        {
            Hotel = null;
            DateStart = null;
            DateEnd = null;
            Country = null;
        }

        public class Date
        {
            public int? Year { get; set; }
            public int? Month { get; set; }
            public int? Day { get; set; }

            public Date()
            {
                Year = null;
                Month = null;
                Day = null;
            }

            public override string ToString()
            {
                if (Year is not null && Month is not null && Day is not null)
                {
                    return new DateTime((int)Year, (int)Month, (int)Day).ToString("yyyy-MM-dd");
                }
                else
                {
                    throw new Exception("SOMETHING HERE");
                }
            }
        }
    }

    public class Sorting
    {
        public bool SortByDate { get; set; }

        public Sorting()
        {
            SortByDate = false;
        }
    }

    public class Pagination
    {
        //подумать над дефолтными значениями
        public int Limit { get; set; }
        public int Offset { get; set; }

        public Pagination()
        {
            Limit = -1;
            Offset = -1;
        }
    }

    public Parameters RequestParameters { get; set; } = new ();
    public Sorting RequestSorting { get; set; } = new ();
    public Pagination RequestPagination { get; set; } = new ();

    /*
    public FilterRequest()
    {
        RequestParameters = new Parameters();
        RequestSorting = new Sorting();
        RequestPagination = new Pagination();
    }
    */
}