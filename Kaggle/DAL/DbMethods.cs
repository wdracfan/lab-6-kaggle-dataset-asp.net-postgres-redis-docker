﻿using Dapper;
using Npgsql;

namespace Kaggle.DAL;

public static class DbMethods
{
    public static async Task<int> LoadDatabase(string connectionString, NpgsqlConnection conn, int numberOfLines)
    {
        try
        {
            Migrations.MyMigrations.DoMigration(connectionString, down: true);
            Migrations.MyMigrations.DoMigration(connectionString, down: false);
            using (var sr = new StreamReader("hotel_bookings.csv"))
            {
                await sr.ReadLineAsync();
                var cnt = 0;
                var anotherCnt = 0;
                var list = new List<Data>();
                while (cnt < numberOfLines && sr.EndOfStream is false)
                {
                    var t = sr.ReadLine();
                    list.Add(new Data(t.Split(',')));
                    cnt++;
                    anotherCnt++;
                    if (anotherCnt == 1000)
                    {
                        await conn.ExecuteAsync($"INSERT INTO dataset.dataset values("
                                                + string.Join(",",
                                                    typeof(Data).GetProperties().Select(x => $"@{x.Name}"))
                                                + ")", list);
                        list = new List<Data>();
                        anotherCnt = 0;
                    }
                }
                await conn.ExecuteAsync($"INSERT INTO dataset.dataset values("
                                        + string.Join(",",
                                            typeof(Data).GetProperties().Select(x => $"@{x.Name}"))
                                        + ")", list);
                
                return cnt;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return -1;
        }
    }

    public static async Task<FilterResponse> Filter(FilterRequest request, NpgsqlConnection conn)
    {
        var parameters = request.RequestParameters;
        var req = @"SELECT * FROM dataset.dataset WHERE TRUE ";
        
        if (parameters.Hotel is not null)
        {
            req += @$"AND hotel = {parameters.Hotel} ";
        }

        if (parameters.Country is not null)
        {
            req += $@"AND country = '{parameters.Country}' ";
        }

        if (parameters.DateStart?.Day is not null 
            && parameters.DateStart?.Month is not null 
            && parameters.DateStart?.Year is not null) //ошибка
        {
            req += $@"AND arrival_date >= '{parameters.DateStart:yyyy-MM-dd}'::date ";
        }
        
        if (parameters.DateEnd?.Day is not null 
            && parameters.DateEnd?.Month is not null 
            && parameters.DateEnd?.Year is not null) //ошибка
        {
            req += $@"AND arrival_date <= '{parameters.DateEnd:yyyy-MM-dd}'::date ";
        }

        if (request.RequestSorting.SortByDate)
        {
            req += "ORDER BY arrival_date ";
        }

        if (request.RequestPagination.Limit != -1)
        {
            req += $"LIMIT {request.RequestPagination.Limit} ";
        }
        
        if (request.RequestPagination.Offset != -1)
        {
            req += $"OFFSET {request.RequestPagination.Offset} ";
        }

        return new FilterResponse(await conn.QueryAsync<Data>(req));
    }

    public static async Task<ReportResponse> ReportReservation(ReportRequest request, NpgsqlConnection conn)
    {
        var req = $@"SELECT reservation_status, reservation_status_date FROM dataset.dataset WHERE TRUE ";

        if (request.DateStart?.Month is not null && request.DateStart.Year is not null)
        {
            req += $"AND to_char(reservation_status_date,'YYYY-MM') >= '{request.DateStart}' ";
        }
        
        if (request.DateEnd?.Month is not null && request.DateEnd.Year is not null)
        {
            req += $"AND to_char(reservation_status_date,'YYYY-MM') <= '{request.DateEnd}' ";
        }

        return new ReportResponse(await conn.QueryAsync<ReservationModel>(req));
    }
    
    public static async Task<ReportResponse> ReportPeople(ReportRequest request, NpgsqlConnection conn)
    {
        var req = $@"SELECT adults, children, babies, arrival_date FROM dataset.dataset WHERE is_canceled = 0 ";

        if (request.DateStart?.Month is not null && request.DateStart.Year is not null)
        {
            req += $"AND to_char(arrival_date,'YYYY-MM') >= '{request.DateStart}' ";
        }
        
        if (request.DateEnd?.Month is not null && request.DateEnd.Year is not null)
        {
            req += $"AND to_char(arrival_date,'YYYY-MM') <= '{request.DateEnd}' ";
        }

        return new ReportResponse(await conn.QueryAsync<PeopleModel>(req));
    }
}