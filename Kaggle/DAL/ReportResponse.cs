﻿namespace Kaggle.DAL;

public class ReportResponse
{
    public SortedDictionary<string, Dictionary<string, int>> Response { get; set; }

    public ReportResponse()
    {
        //for deserialization
    }
    
    public ReportResponse(IEnumerable<ReservationModel> model)
    {
        Response = new SortedDictionary<string, Dictionary<string, int>>();
        foreach (var i in model)
        {
            var date = i.reservation_status_date.ToString("yyyy-MM");
            var status = i.reservation_status == 1 ? "Check-Out" : "Canceled";
            if (Response.ContainsKey(date) is false)
            {
                Response.Add(date,
                    new Dictionary<string, int>());
            }

            if (Response[date].ContainsKey(status))
            {
                Response[date][status]++;
            }
            else
            {
                Response[date].Add(status, 1);
            }
        }
    }

    public ReportResponse(IEnumerable<PeopleModel> model)
    {
        Response = new SortedDictionary<string, Dictionary<string, int>>();
        foreach (var i in model)
        {
            var date = i.arrival_date.ToString("yyyy-MM");
            if (Response.ContainsKey(date) is false)
            {
                Response.Add(date,
                    new Dictionary<string, int>());
                Response[date].Add("Adults",0);
                Response[date].Add("Children",0);
            }

            Response[date]["Adults"] += i.adults;
            Response[date]["Children"] += i.children + i.babies;
        }
    }
}