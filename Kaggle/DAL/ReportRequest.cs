﻿namespace Kaggle.DAL;

public class ReportRequest
{
    public Date? DateStart { get; set; }
    public Date? DateEnd { get; set; }

    public class Date
    {
        public int? Month { get; set; }
        public int? Year { get; set; }
        
        public Date()
        {
            Year = null;
            Month = null;
        }

        public override string ToString()
        {
            if (Year is not null && Month is not null)
            {
                return new DateTime((int)Year, (int)Month, 10).ToString("yyyy-MM");
            }
            else
            {
                throw new Exception("SOMETHING HERE");
            }
        }
    }
}