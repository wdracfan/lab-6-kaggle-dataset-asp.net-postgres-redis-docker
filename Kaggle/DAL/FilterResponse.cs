﻿namespace Kaggle.DAL;

public class FilterResponse
{
    public List<Data> Response { get; set; }
    
    public FilterResponse()
    {
        //
    }

    public FilterResponse(IEnumerable<Data> response)
    {
        Response = response.ToList();
    }
    
}