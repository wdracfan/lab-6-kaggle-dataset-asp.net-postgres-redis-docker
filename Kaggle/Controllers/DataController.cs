using System.Text.Json;
using Kaggle.DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Npgsql;
using StackExchange.Redis;

namespace Kaggle.Controllers;

[ApiController]
[Route("[controller]")]
public class DataController : ControllerBase
{
    private readonly string _connectionString;
    private readonly NpgsqlConnection _conn;
    private readonly IDatabase _redisDb;
    private readonly IServer _redisServer;

    public DataController(IOptions<PostgresOptions> postgresOptions, IOptions<RedisOptions> redisOptions)
    {
        var connectionString = postgresOptions.Value.ConnectionString;
        _connectionString = connectionString;
        _conn = new NpgsqlConnection(connectionString);

        var connectionStringRedis = redisOptions.Value.ConnectionString;
        var connectionRedis = ConnectionMultiplexer.Connect(connectionStringRedis);
        _redisDb = connectionRedis.GetDatabase();
        _redisServer = connectionRedis.GetServer(connectionRedis.GetEndPoints()[0]);

    }


    [HttpPost]
    [Route("load")]
    public async Task<IResult> LoadHandler(int numberOfLines)
    {
        var res = await DbMethods.LoadDatabase(_connectionString, _conn, numberOfLines);
        if (res == -1)
        {
            return Results.BadRequest("Some unexpected error occured.");
        }
        else
        {
            await Redis.ClearRedis(_redisServer);
            return Results.Ok($"{res} lines have been successfully loaded.");
        }
    }

    [HttpPost]
    [Route("filter")]
    public async Task<IResult> FilterHandler([FromQuery] FilterRequest request)
    {
        var req = new
        {
            type = "Filter",
            request,
        };
        var cached = await Redis.CheckRedis(_redisDb, req);
        if (cached == "")
        {
            try
            {
                var response = await DbMethods.Filter(request, _conn);
                await Redis.CacheRedis(_redisDb, req, response);
                return Results.Ok(response);
            }
            catch
            {
                return Results.BadRequest("Some unexpected error occured.");
            }
        }
        else
        {
            return Results.Ok(JsonSerializer.Deserialize<FilterResponse>(cached));
        }
    }

    [HttpPost]
    [Route("report/reservation")]
    public async Task<IResult> ReportReservationHandler([FromQuery] ReportRequest request)
    {
        var req = new
        {
            type = "Reservation",
            request,
        };
        var cached = await Redis.CheckRedis(_redisDb, req);
        if (cached == "")
        {
            try
            {
                var response = await DbMethods.ReportReservation(request, _conn);
                await Redis.CacheRedis(_redisDb, req, response);
                return Results.Ok(response);
            }
            catch
            {
                return Results.BadRequest("Some unexpected error occured.");
            }
        }
        else
        {
            return Results.Ok(JsonSerializer.Deserialize<ReportResponse>(cached));
        }
    }
    
    [HttpPost]
    [Route("report/people")]
    public async Task<IResult> ReportPeopleHandler([FromQuery] ReportRequest request)
    {
        var req = new
        {
            type = "People",
            request,
        };
        var cached = await Redis.CheckRedis(_redisDb, req);
        if (cached == "")
        {
            try
            {
                var response = await DbMethods.ReportPeople(request, _conn);
                await Redis.CacheRedis(_redisDb, req, response);
                return Results.Ok(response);
            }
            catch
            {
                return Results.BadRequest("Some unexpected error occured.");
            }
        }
        else
        {
            return Results.Ok(JsonSerializer.Deserialize<ReportResponse>(cached));
        }
    }
}