﻿FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["Kaggle/Kaggle.csproj", "Kaggle/"]
RUN dotnet restore "Kaggle/Kaggle.csproj"
WORKDIR "/src/Kaggle"
COPY . .
RUN dotnet build "Kaggle/Kaggle.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Kaggle/Kaggle.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
COPY Kaggle/hotel_bookings.csv .
ENTRYPOINT ["dotnet", "Kaggle.dll"]
