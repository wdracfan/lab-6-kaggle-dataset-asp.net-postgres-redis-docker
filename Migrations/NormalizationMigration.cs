﻿using FluentMigrator;

namespace Migrations;

[Migration(2)]
public class NormalizationMigration : Migration
{
    public override void Up()
    {
        Create.Table("hotel_types").InSchema("dataset")
            .WithColumn("id").AsInt32().PrimaryKey().Identity()
            .WithColumn("hotel_type").AsString();
        Insert.IntoTable("hotel_types").InSchema("dataset")
            .Row(new { hotel_type = "Resort Hotel" })
            .Row(new { hotel_type = "City Hotel" });
        
        Create.Table("meal_types").InSchema("dataset")
            .WithColumn("id").AsInt32().PrimaryKey().Identity()
            .WithColumn("meal_type").AsString();
        Insert.IntoTable("meal_types").InSchema("dataset")
            .Row(new { meal_type = "BB" })
            .Row(new { meal_type = "HB" })
            .Row(new { meal_type = "FB" });
        
        Create.Table("market_segment_types").InSchema("dataset")
            .WithColumn("id").AsInt32().PrimaryKey().Identity()
            .WithColumn("market_segment_type").AsString();
        Insert.IntoTable("market_segment_types").InSchema("dataset")
            .Row(new { market_segment_type = "Direct" })
            .Row(new { market_segment_type = "Corporate" })
            .Row(new { market_segment_type = "Online TA" })
            .Row(new { market_segment_type = "Offline TA/TO" });
        
        Create.Table("distribution_channel_types").InSchema("dataset")
            .WithColumn("id").AsInt32().PrimaryKey().Identity()
            .WithColumn("distribution_channel_type").AsString();
        Insert.IntoTable("distribution_channel_types").InSchema("dataset")
            .Row(new { distribution_channel_type = "Direct" })
            .Row(new { distribution_channel_type = "Corporate" })
            .Row(new { distribution_channel_type = "TA/TO" });
        
        Create.Table("deposit_types").InSchema("dataset")
            .WithColumn("id").AsInt32().PrimaryKey().Identity()
            .WithColumn("deposit_type").AsString();
        Insert.IntoTable("deposit_types").InSchema("dataset")
            .Row(new { deposit_type = "No Deposit" })
            .Row(new { deposit_type = "Non Refund" });
        
        Create.Table("customer_types").InSchema("dataset")
            .WithColumn("id").AsInt32().PrimaryKey().Identity()
            .WithColumn("customer_type").AsString();
        Insert.IntoTable("customer_types").InSchema("dataset")
            .Row(new { customer_type = "Transient" })
            .Row(new { customer_type = "Transient-Party" })
            .Row(new { customer_type = "Contract" });
        
        Create.Table("reservation_status_types").InSchema("dataset")
            .WithColumn("id").AsInt32().PrimaryKey().Identity()
            .WithColumn("reservation_status_type").AsString();
        Insert.IntoTable("reservation_status_types").InSchema("dataset")
            .Row(new { reservation_status_type = "Check-Out" })
            .Row(new { reservation_status_type = "Canceled" });
    }

    public override void Down()
    {
        Delete.Table("hotel_types").InSchema("dataset");
        Delete.Table("meal_types").InSchema("dataset");
        Delete.Table("market_segment_types").InSchema("dataset");
        Delete.Table("distribution_channel_types").InSchema("dataset");
        Delete.Table("deposit_types").InSchema("dataset");
        Delete.Table("customer_types").InSchema("dataset");
        Delete.Table("reservation_status_types").InSchema("dataset");
    }
}