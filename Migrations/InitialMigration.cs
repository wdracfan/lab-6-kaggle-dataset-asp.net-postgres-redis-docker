﻿using FluentMigrator;

namespace Migrations;

[Migration(1)]
public class InitialMigration : Migration
{
    public override void Up()
    {
        Create.Schema("dataset");

        Create.Table("dataset").InSchema("dataset")
            .WithColumn("hotel").AsInt32()
            .WithColumn("is_canceled").AsInt32() //bool
            .WithColumn("lead_time").AsInt32()
            //.WithColumn("arrival_date_year").AsInt32()
            //.WithColumn("arrival_date_month").AsString()
            //.WithColumn("arrival_date_week_number").AsInt32()
            //.WithColumn("arrival_date_day_of_month").AsInt32()
            .WithColumn("arrival_date").AsDateTime()
            .WithColumn("stays_in_weekend_nights").AsInt32()
            .WithColumn("stays_in_week_nights").AsInt32()
            .WithColumn("adults").AsInt32().Nullable()
            .WithColumn("children").AsInt32().Nullable()
            .WithColumn("babies").AsInt32().Nullable()
            .WithColumn("meal").AsInt32()
            .WithColumn("country").AsString()
            .WithColumn("market_segment").AsInt32()
            .WithColumn("distribution_channel").AsInt32()
            .WithColumn("is_repeated_guest").AsInt32() //bool
            .WithColumn("previous_cancellations").AsString()
            .WithColumn("previous_bookings_not_canceled").AsInt32()
            .WithColumn("reserved_room_type").AsString()
            .WithColumn("assigned_room_type").AsString()
            .WithColumn("booking_changes").AsInt32()
            .WithColumn("deposit_type").AsInt32()
            .WithColumn("agent").AsInt32().Nullable()
            .WithColumn("company").AsInt32().Nullable()
            .WithColumn("days_in_waiting_list").AsInt32()
            .WithColumn("customer_type").AsInt32()
            .WithColumn("adr").AsDouble()
            .WithColumn("required_car_parking_spaces").AsInt32()
            .WithColumn("total_of_special_requests").AsInt32()
            .WithColumn("reservation_status").AsInt32()
            .WithColumn("reservation_status_date").AsDateTime();
    }

    public override void Down()
    {
        Delete.Table("dataset").InSchema("dataset");
        
        Delete.Schema("dataset");
        
        //Delete.Table("VersionInfo").InSchema("public");
    }
}